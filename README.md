# Pixel

Pixel is a free and open source texture pack for Minecraft.
Due to the fact that it's open source, the community can directly
give feedback, which makes this the perfect community Texture Pack.
The Texturepack currently only correctly works with Version 1.12.2.
An upgrade to a newer version is excpected, even though it may take a while.

# Why do you make it Open Source
Because then we can directly interact with the community, and so do they with us.
It's better when the Community is integrated in order to share ideas, problems etc.
Closed Source Software may have it's advantages, but I think it's the right way to
go into the Open Source Direction.

# Current Progression
The **master branch** is not considered to be unstable, it's rather a not finished
and polished version of the product we're currently working on. **master** is were everything
new goes and may be improved. The stable branch will contain a rather more finished and polished
version of the texture pack.

I am currently working on a rework for (most likely) **all** textures. That's right. We're doing a complete new texture pack.
I will include some already finished textures that I created, but they might be replaced by better versions.

# Release state
The Texture Pack is a in-developement Product.

# How can I use it?
You can use it by downloading it as a zip, and extracting the folder into your ressourcepacks folder.
The texture pack looks as following:

![Preview 1](https://i.imgur.com/HA8YBsO.png)